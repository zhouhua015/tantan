package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"
)

var testingNames = [...]string{
	`Alice`, `Bob`, `Charlie`, `David`, `Elton`, `Fin`, `George`,
	`Harry`, `Ian`, `Jesse`, `Karl`, `Leon`, `Mike`, `Nathon`,
	`Oscar`, `Pratt`, `Quinn`, `Russel`, `Scotto`, `Ted`,
	`Uri`, `Vincent`, `Walter`, `Xeno`, `Yonas`, `Zack`,
}

func insertRandomRs(user *User, count int) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < count; i++ {
		state := r.Int()%StateMatched + 1
		uId := uint64(r.Int63())
		pId := uint64(r.Int63())
		randomRs := &Relationship{UserId: uId, PeerId: pId, State: state}
		user.insertRs2User(randomRs)
	}
}

func TestUser_findRsInCache(t *testing.T) {
	user := &User{Id: 1234, Name: "Walter White", Typ: TYPE_User}
	peer := &User{Id: 4321, Name: "Jesse Pinkman", Typ: TYPE_User}
	rs := &Relationship{UserId: user.Id, PeerId: peer.Id, State: StateDisliked}
	user.insertRs2User(rs)

	foundRs := user.findRsInCache(rs)
	if foundRs == nil {
		t.Fatal("Relationship rs (%d - %d - %d : %d : %d) cannot be found\n",
			foundRs.PeerId, foundRs.State, foundRs.UserId,
			foundRs.time, foundRs.count)
	}

	idRs := &Relationship{UserId: 2345, PeerId: 5432, State: StateLiked}
	foundRs = user.findRsInCache(idRs)
	if foundRs != nil {
		t.Fatal("Ghost relationship (%d - %d - %d : %d : %d) found\n",
			foundRs.PeerId, foundRs.State, foundRs.UserId,
			foundRs.time, foundRs.count)
	}
}

func TestUser_insertRs2User(t *testing.T) {
	user := &User{Id: 1234, Name: "Walter White", Typ: TYPE_User}
	peer := &User{Id: 4321, Name: "Jesse Pinkman", Typ: TYPE_User}
	rs := &Relationship{UserId: user.Id, PeerId: peer.Id, State: StateDisliked}

	user.insertRs2User(rs)
	var found bool = false
	for _, tmpRs := range user.rss {
		if tmpRs.PeerId == rs.PeerId {
			found = true
		}
	}
	if !found {
		t.Fatal("Inserted rs disappeared")
	}

	insertRandomRs(user, RS_MRU_COUNT)
	if user.rss.Len() > RS_MRU_COUNT {
		t.Fatalf("Failed MRU relationship cache size control, got %d\n",
			user.rss.Len())
	}

	found = false
	for _, tmpRs := range user.rss {
		if tmpRs.PeerId == rs.PeerId {
			found = true
		}
	}
	if found {
		t.Fatal("Relationship rs (%d - %d - %d : %d : %d) is still there\n",
			rs.PeerId, rs.State, rs.UserId, rs.time, rs.count)
	}
}

func TestUser_updateRsCache(t *testing.T) {
	user := &User{Id: 1234, Name: "Walter White", Typ: TYPE_User}
	peer := &User{Id: 4321, Name: "Jesse Pinkman", Typ: TYPE_User}
	rs := &Relationship{UserId: user.Id, PeerId: peer.Id, State: StateDisliked}
	user.insertRs2User(rs)

	insertRandomRs(user, RS_MRU_COUNT-1)
	if user.rss.Len() > RS_MRU_COUNT {
		t.Fatal("Failed MRU relationship cache size control, got %d\n",
			user.rss.Len())
	}
	rs.State = StateMatched
	user.updateRsCache(rs)

	last := &Relationship{UserId: 5432, PeerId: 2345, State: StateDisliked}
	user.insertRs2User(last)

	var found bool = false
	for _, tmpRs := range user.rss {
		if tmpRs.PeerId == rs.PeerId {
			found = true
		}
	}
	if !found {
		t.Fatal("Relationship rs (%d - %d - %d : %d : %d) disappeared\n",
			rs.PeerId, rs.State, rs.UserId, rs.time, rs.count)
	}
}

func verifyNewUserManagerFromEmpty() {
	_, err := connMngr.Conn.ExecOne("TRUNCATE users CASCADE")
	if err != nil {
		log.Fatal("Cannot clear table 'users'", err)
	}
	userMngr = NewUserManager(connMngr)
	if userMngr.cache.Len() != 0 {
		log.Fatal("New user manager from clean database must have NO data")
	}
	if len(userMngr.names) != 0 {
		log.Fatal("New user manager from clean database must have NO data")
	}
	if len(userMngr.ids) != 0 {
		log.Fatal("New user manager from clean database must have NO data")
	}
}

func verifyNewUserManager() {
	_, err := connMngr.Conn.ExecOne("TRUNCATE users CASCADE")
	if err != nil {
		log.Fatal("Cannot clear table 'users'", err)
	}

	names := testingNames[0:16]
	for _, name := range names {
		var dummy User
		connMngr.Conn.QueryOne(dummy, `
			INSERT INTO users (name) VALUES (?)
			RETURNING id
		`, name)
	}

	userMngr = NewUserManager(connMngr)
	if userMngr.cache.Len() == 0 ||
		len(userMngr.names) == 0 ||
		len(userMngr.ids) == 0 {
		log.Fatal("UserManager is supposed to load (at least partial) users from DB")
	}
	if userMngr.cache.Len() != len(names) ||
		len(userMngr.names) != len(names) ||
		len(userMngr.ids) != len(names) {
		log.Fatal("UserManager does NOT load enough users")
	}
	if userMngr.cache.Len() != len(userMngr.names) ||
		userMngr.cache.Len() != len(userMngr.ids) ||
		len(userMngr.names) != len(userMngr.ids) {
		log.Fatal("UserManager got mismatched internal data structure")
	}
}

func verifyUserManager_GetUsers_FromEmpty() {
	_, err := connMngr.Conn.ExecOne("TRUNCATE users CASCADE")
	if err != nil {
		log.Fatal("Cannot clear table 'users'", err)
	}

	userMngr = NewUserManager(connMngr)
	users, err := userMngr.GetUsers()
	if len(users) != 0 {
		log.Fatal("GetUsers() from an empty DB should alwasy return 0")
	}
}

func verifyUserManager_GetUsers() {
	_, err := connMngr.Conn.ExecOne("TRUNCATE users CASCADE")
	if err != nil {
		log.Fatal("Cannot clear table 'users'", err)
	}

	for _, name := range testingNames {
		var dummy User
		connMngr.Conn.QueryOne(dummy, `
			INSERT INTO users (name) VALUES (?)
			RETURNING id
		`, name)
	}

	userMngr = NewUserManager(connMngr)
	users, err := userMngr.GetUsers()
	if len(users) > MAX_RETURN_ITEM_COUNT {
		log.Fatalf("GetUsers() should alwasy return less than %d users, but got %d.\n",
			MAX_RETURN_ITEM_COUNT, len(users))
	}
}

func insertRandomUser(mngr *UserManager, count int) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < count; i++ {

		index := r.Uint32() % uint32(len(testingNames))
		name := testingNames[index]
		name += strconv.Itoa(i)
		uId := uint64(r.Int63())
		user := &User{Id: uId, Name: name}
		mngr.insertUser2Cache(user)
	}
}

func TestUserManager_insertUser2Cache(t *testing.T) {
	user := &User{Id: 1234, Name: "Walter White", Typ: TYPE_User}
	userMngr.insertUser2Cache(user)

	var found bool = false
	for _, tmpUser := range userMngr.cache {
		if tmpUser.Id == user.Id {
			found = true
		}
	}
	if !found {
		t.Fatal("Inserted user disappeared")
	}

	insertRandomUser(userMngr, USER_CACHE_SIZE)
	if userMngr.cache.Len() > USER_CACHE_SIZE {
		t.Fatal("Failed MRU user cache size control, got %d\n",
			userMngr.cache.Len())
	}

	found = false
	for _, tmpUser := range userMngr.cache {
		if tmpUser.Id == user.Id {
			found = true
		}
	}
	if found {
		t.Fatal("User %s is still there\n", user.Name)
	}
}

func TestUserManager_updateUserCache(t *testing.T) {
	user := &User{Id: 1234, Name: "Walter White", Typ: TYPE_User}
	userMngr.insertUser2Cache(user)

	insertRandomUser(userMngr, USER_CACHE_SIZE-1)
	if userMngr.cache.Len() > USER_CACHE_SIZE {
		t.Fatal("Failed MRU user cache size control, got %d\n",
			userMngr.cache.Len())
	}
	user.time = time.Now().UnixNano()
	userMngr.updateUserCache(user)

	last := &User{Id: 4321, Name: "Jesse Pinkman", Typ: TYPE_User}
	userMngr.insertUser2Cache(last)

	var found bool = false
	for _, tmpUser := range userMngr.cache {
		if tmpUser.Id == user.Id {
			found = true
		}
	}
	if !found {
		t.Fatal("User %s disappeared\n", user.Name)
	}
}

func TestUserManager_IsUserExist(t *testing.T) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	index := r.Uint32() % uint32(userMngr.cache.Len())
	user := &User{Name: userMngr.cache[index].Name}
	if ok := userMngr.IsUserExist(user); !ok {
		t.Fatalf("%s is supposed to be there.\n", userMngr.cache[index].Name)
	}
	user = &User{Name: "Gus"}
	if ok := userMngr.IsUserExist(user); ok {
		t.Fatal("Gus is NOT supposed to be there.")
	}
}

func TestUserManager_AddUser(t *testing.T) {
	newNames := []string{`Lester`, `Molly`, `Malvo`}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	index := r.Uint32() % uint32(len(newNames))
	user := &User{Name: newNames[index]}
	user, err := userMngr.AddUser(user)
	if err != nil {
		t.Fatal("AddUser raised an error, ", err)
	}
	if user.Name != newNames[index] {
		t.Fatalf("AddUser() returned a wrong name, expect %s got %s.\n",
			newNames[index], user.Name)
	}
	if user.count != 1 {
		t.Fatal("The used count of newly added user should be 1")
	}

	if userMngr.cache[user.index].Name != newNames[index] {
		t.Fatal("New added user is not loaded into cache correctly")
	}
	if _, ok := userMngr.names[newNames[index]]; !ok {
		t.Fatal("New added user is not loaded into name map correctly")
	}

	// Duplicate user adding
	_, err = userMngr.AddUser(user)
	if err == nil {
		t.Fatal("Expected error disappeared, duplicate user cannot be inserted")
	}

	// Flush all old users
	var newTestingNames = [...]string{
		`Abe`, `Baker`, `Cary`, `Duncan`, `Emma`, `Frank`, `Glin`,
		`Hillary`, `Irvyn`, `Jason`, `Kate`, `Lily`, `Michel`, `Neo`,
		`Obrian`, `Pears`, `Qeeto`, `Ross`, `Sara`, `Tony`,
		`Unai`, `Victor`, `Wendy`, `Xzavier`, `Yoseif`, `Zoe`,
	}

	for i := 0; i < USER_CACHE_SIZE; i++ {
		index := r.Uint32() % uint32(len(newNames))
		tmpName := newTestingNames[index]
		tmpName += strconv.Itoa(i)
		user = &User{Name: tmpName}
		_, err = userMngr.AddUser(user)
		if err != nil {
			t.Fatalf("AddUser() raised an error: %s while user(%s) added",
				err.Error(), tmpName)
		}
	}

	for _, name := range testingNames {
		if left, ok := userMngr.names[name]; ok {
			t.Errorf("User %s (count: %d, time: %d) still in name map\n",
				name, left.count, left.time)
		}

		for _, tmp := range userMngr.cache {
			if tmp.Name == name {
				t.Errorf("User %s (count: %d, time: %d) still in cache\n",
					name, tmp.count, tmp.time)
			}
		}
	}
}

const (
	testingUserId = 987654321
	testingPeerId = 123456789
)

var user *User = &User{Id: testingUserId, Name: "Walter White", Typ: TYPE_User}
var peer *User = &User{Id: testingPeerId, Name: "Jesse Pinkman", Typ: TYPE_User}

func setup4MatchBreakup(t *testing.T) {
	_, err := connMngr.Conn.Exec(`
		INSERT INTO users (id, name) VALUES (?id, ?name)
	`, user)
	if err != nil {
		t.Fatalf("Failed to insert user, %s", err.Error())
	}
	_, err = connMngr.Conn.Exec(`
		INSERT INTO users (id, name) VALUES (?id, ?name)
	`, peer)
	if err != nil {
		t.Fatalf("Failed to insert peer, %s", err.Error())
	}

	_, err = connMngr.Conn.Exec(`
		INSERT INTO relationships (peer_id, state, user_id) VALUES (?, ?, ?)
	`, testingPeerId, StateLiked, testingUserId)
	if err != nil {
		t.Fatalf("Failed to insert relationship, %s", err.Error())
	}
}

func teardown4MatchBreakup(t *testing.T) {
	_, err := connMngr.Conn.Exec(`
		DELETE FROM relationships 
			WHERE (peer_id=(?) AND user_id=(?)) OR (user_id=(?) AND peer_id=(?))
	`, testingPeerId, testingUserId, testingPeerId, testingUserId)
	if err != nil {
		t.Fatal("Failed to cleanup relationship, %s", err.Error())
	}
	_, err = connMngr.Conn.Exec("DELETE FROM users WHERE id=(?id)", user)
	if err != nil {
		t.Fatal("Failed to cleanup user, %s", err.Error())
	}
	_, err = connMngr.Conn.Exec("DELETE FROM users WHERE id=(?id)", peer)
	if err != nil {
		t.Fatal("Failed to cleanup peer, %s", err.Error())
	}
}

func TestUserManager_match_and_breakupPeerRs(t *testing.T) {
	setup4MatchBreakup(t)

	u := &User{Id: testingPeerId}
	rs := &Relationship{PeerId: testingUserId, State: StateLiked}
	matchedRs, ok := userMngr.matchPeerRs(u, rs)
	if ok != (matchedRs.State == StateMatched) {
		t.Fatal("Conflict return values")
	}
	if !ok {
		t.Fatalf("The relationship should be matched, not %d", matchedRs.State)
	}

	var peerRs Relationship
	_, err := connMngr.Conn.QueryOne(&peerRs, `
		SELECT * FROM relationships WHERE peer_id = (?) and user_id=(?)
	`, testingPeerId, testingUserId)
	if err != nil {
		t.Fatalf("Fail to query peer relationship, %s\n", err.Error())
	}
	if peerRs.State != StateMatched {
		t.Fatalf("Peer relationship should be matched, not %d\n", peerRs.State)
	}

	rs = &Relationship{PeerId: testingUserId, State: StateDisliked}
	damagedRs, ok := userMngr.breakupPeerRs(u, rs)
	if !ok {
		t.Fatal("The breakup operation should be sucessful")
	}
	if damagedRs.State != rs.State {
		t.Fatalf("The relation after breakup should remain as %d, not %s\n",
			rs.State, damagedRs.State)
	}

	_, err = connMngr.Conn.QueryOne(&peerRs, `
		SELECT * FROM relationships WHERE peer_id = (?) and user_id=(?)
	`, testingPeerId, testingUserId)
	if err != nil {
		t.Fatalf("Fail to query peer relationship, %s\n", err.Error())
	}
	if peerRs.State != StateLiked {
		t.Fatalf("Peer relationship should be liked, not %d\n", peerRs.State)
	}

	teardown4MatchBreakup(t)
}

func TestMain(m *testing.M) {
	flag.Parse()

	connMngr = NewDBConnManager("conf/db.json")

	verifyNewUserManagerFromEmpty()
	verifyNewUserManager()
	verifyUserManager_GetUsers_FromEmpty()
	verifyUserManager_GetUsers()

	// Setup users table for regular testings
	_, err := connMngr.Conn.ExecOne("TRUNCATE users CASCADE")
	if err != nil {
		log.Fatal("Cannot clear table 'users'", err)
	}

	names := testingNames[0:16]
	for _, name := range names {
		var dummy User
		connMngr.Conn.QueryOne(dummy, `
			INSERT INTO users (name) VALUES (?)
			RETURNING id
		`, name)
	}
	userMngr = NewUserManager(connMngr)

	os.Exit(m.Run())
}
