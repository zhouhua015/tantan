DROP TABLE IF EXISTS users CASCADE;
CREATE TABLE IF NOT EXISTS users (
    id      bigserial,          
    name    VARCHAR NOT NULL,

    PRIMARY KEY(id)
);

DROP TABLE IF EXISTS relationships CASCADE;
CREATE TABLE IF NOT EXISTS relationships (
    peer_id BIGINT REFERENCES users(id),
    state   INT,
    user_id BIGINT REFERENCES users(id),

    PRIMARY KEY(peer_id, user_id)
);
