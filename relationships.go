package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"strconv"
	"strings"
)

const (
	StateDisliked = iota
	StateLiked
	StateMatched
)

const (
	jsonKeyId    = "id"
	jsonKeyState = "state"
	jsonKeyTyp   = "type"

	jsonValueStateDislike = "disliked"
	jsonValueStateLike    = "liked"
	jsonValueStateMatched = "matched"
)

type Relationship struct {
	PeerId uint64
	State  int
	Typ    string
	UserId uint64

	time  int64
	count uint64
	index int
}

func (rs *Relationship) MarshalJSON() ([]byte, error) {
	bytes := bytes.NewBuffer([]byte("{"))
	bytes.WriteString("\"" + jsonKeyId + "\":\"")
	bytes.WriteString(strconv.FormatUint(rs.PeerId, 10))
	bytes.WriteString("\",")

	bytes.WriteString("\"" + jsonKeyState + "\":\"")
	switch rs.State {
	default:
		return nil, errors.New("Invalid Relationship instance")
	case StateDisliked:
		bytes.WriteString(jsonValueStateDislike)
	case StateLiked:
		bytes.WriteString(jsonValueStateLike)
	case StateMatched:
		bytes.WriteString(jsonValueStateMatched)
	}
	bytes.WriteString("\",")

	bytes.WriteString("\"" + jsonKeyTyp + "\":\"")
	bytes.WriteString(rs.Typ)
	bytes.WriteString("\"")

	bytes.WriteString("}")

	return bytes.Bytes(), nil
}

func (rs *Relationship) UnmarshalJSON(data []byte) error {
	v := make(map[string]string)
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	if _, ok := v[jsonKeyId]; ok {
		peerId, err := strconv.ParseUint(v[jsonKeyId], 10, 64)
		if err != nil {
			return errors.New("invalid Relationship json, parse " + jsonKeyId)
		}
		rs.PeerId = peerId
	}

	if _, ok := v[jsonKeyState]; ok {
		switch strings.ToLower(v[jsonKeyState]) {
		default:
			return errors.New("invalid Relationship json, parse " + jsonKeyState)
		case jsonValueStateMatched:
			rs.State = StateMatched
		case jsonValueStateLike:
			rs.State = StateLiked
		case jsonValueStateDislike:
			rs.State = StateDisliked
		}
	}

	if _, ok := v[jsonKeyTyp]; ok {
		rs.Typ = v[jsonKeyTyp]
	}

	return nil
}

type MruRelationships []*Relationship

func (mru MruRelationships) Len() int { return len(mru) }

func (mru MruRelationships) Less(i, j int) bool {
	if mru[i].State < mru[j].State {
		return true
	}
	if mru[j].State < mru[i].State {
		return false
	}

	if mru[i].time < mru[j].time {
		return true
	}
	if mru[j].time < mru[i].time {
		return false
	}

	if mru[i].count < mru[j].count {
		return true
	}
	if mru[j].count < mru[i].count {
		return false
	}

	if mru[i].PeerId < mru[j].PeerId {
		return true
	}

	return false
}

func (mru MruRelationships) Swap(i, j int) {
	mru[i], mru[j] = mru[j], mru[i]
	mru[i].index = i
	mru[j].index = j
}

func (mru *MruRelationships) Push(x interface{}) {
	n := len(*mru)
	relationship := x.(*Relationship)
	relationship.index = n
	*mru = append(*mru, relationship)
}

func (mru *MruRelationships) Pop() interface{} {
	old := *mru
	n := len(old)
	relationship := old[n-1]
	relationship.index = -1 // for safety
	*mru = old[0 : n-1]
	return relationship
}
