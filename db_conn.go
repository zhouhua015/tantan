package main

import (
	"encoding/json"
	"os"

	"github.com/golang/glog"
	"gopkg.in/pg.v3"
)

type DBOptions struct {
	Host     string
	Port     string
	User     string
	Password string
	Db       string
	PoolSize int
}

type DBConnManager struct {
	Conn *pg.DB
}

func NewDBConnManager(confFile string) *DBConnManager {
	connMngr := &DBConnManager{}
	connMngr.Prepare(confFile)

	return connMngr
}

func (connMngr *DBConnManager) Prepare(confFileName string) {
	const maxConfFileSize = 1 * 1024 * 1024
	info, err := os.Stat(confFileName)
	if err != nil {
		glog.Fatalf("Get configuration file info failed, %s\n", err.Error())
	}
	if info.Size() > maxConfFileSize {
		glog.Fatal("Database configuration file corrupted")
	}

	confFile, err := os.Open(confFileName)
	if err != nil {
		glog.Fatal("Cannot open database configuration file")
	}
	defer confFile.Close()

	data := make([]byte, info.Size())
	size, err := confFile.Read(data)
	if err != nil {
		glog.Fatal("Read database configuration file failed")
	}
	if int64(size) != info.Size() {
		glog.Fatal("Read database configuration file failed, cannot read all content")
	}

	var options DBOptions
	// TODO XML instead of JSON ??
	if err = json.Unmarshal(data, &options); err != nil {
		glog.Fatalf("Parse database configuration file failed, %s\n", string(data))
	}

	glog.Info("Database configuration, ", options)

	connMngr.Conn = pg.Connect(&pg.Options{
		Host:     options.Host,
		Port:     options.Port,
		User:     options.User,
		Password: options.Password,
		Database: options.Db,
		PoolSize: options.PoolSize,
	})
}
