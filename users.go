package main

import (
	"container/heap"
	"errors"
	"strconv"
	"time"

	"github.com/golang/glog"
)

const (
	USER_CACHE_SIZE       = 2000
	RS_MRU_COUNT          = 500
	MAX_RETURN_ITEM_COUNT = 20
)

type User struct {
	Id   uint64 `json:"id"`
	Name string `json:"name"`
	Typ  string `json:"type"`

	time  int64  `json:"-"`
	count uint64 `json:"-"`
	index int    `json:"-"`

	rss MruRelationships `json:"-"`
}

func (u *User) findRsInCache(idRs *Relationship) *Relationship {
	if idRs.UserId != u.Id {
		return nil
	}

	for _, rs := range u.rss {
		if rs.PeerId == idRs.PeerId {
			return rs
		}
	}

	return nil
}

func (u *User) insertRs2User(rs *Relationship) {
	rs.time = time.Now().UnixNano()
	rs.count = 1
	if u.rss.Len() >= RS_MRU_COUNT {
		oldRs := heap.Pop(&u.rss).(*Relationship)
		glog.Info("Age-out relationship(id: %d, state: %d) "+
			"for user %s from memory cache.\n",
			oldRs.PeerId, oldRs.State, u.Name)
	}
	heap.Push(&u.rss, rs)
}

func (u *User) updateRsCache(rs *Relationship) {
	rs.count++
	rs.time = time.Now().UnixNano()
	heap.Fix(&u.rss, rs.index)
}

type mruUsers []*User

func (mru mruUsers) Len() int { return len(mru) }

func (mru mruUsers) Less(i, j int) bool {
	if mru[i].time < mru[j].time {
		return true
	}
	if mru[j].time < mru[i].time {
		return false
	}

	if mru[i].count < mru[j].count {
		return true
	}
	if mru[j].count < mru[i].count {
		return false
	}

	if mru[i].Id < mru[j].Id {
		return true
	}

	return false
}

func (mru mruUsers) Swap(i, j int) {
	mru[i], mru[j] = mru[j], mru[i]
	mru[i].index = i
	mru[j].index = j
}

func (mru *mruUsers) Push(x interface{}) {
	n := len(*mru)
	user := x.(*User)
	user.index = n
	*mru = append(*mru, user)
}

func (mru *mruUsers) Pop() interface{} {
	old := *mru
	n := len(old)
	user := old[n-1]
	user.index = -1 // for safety
	*mru = old[0 : n-1]
	return user
}

type UserManager struct {
	connMngr *DBConnManager

	cache mruUsers
	names map[string]*User
	ids   map[uint64]*User
}

func NewUserManager(connMngr *DBConnManager) *UserManager {
	userMngr := &UserManager{connMngr: connMngr}

	var users []User
	_, err := connMngr.Conn.Query(&users, `
		SELECT * FROM users LIMIT (?)
	`, USER_CACHE_SIZE)
	if err != nil {
		glog.Fatal("Failed to pre-fetch users, ", err)
	}

	cacheSize := len(users)
	if len(users) >= USER_CACHE_SIZE {
		cacheSize = USER_CACHE_SIZE
	}

	userMngr.names = make(map[string]*User)
	userMngr.ids = make(map[uint64]*User)
	userMngr.cache = make(mruUsers, cacheSize)
	for i := 0; i < len(users); i++ {
		userMngr.cache[i] = &users[i]
		userMngr.cache[i].Typ = TYPE_User
		userMngr.cache[i].count = 1
		userMngr.cache[i].index = i
		userMngr.cache[i].time = time.Now().UnixNano()

		userMngr.names[users[i].Name] = &users[i]
		userMngr.ids[users[i].Id] = &users[i]

		userMngr.getRss4User(&users[i])
	}
	heap.Init(&userMngr.cache)

	return userMngr
}

func (mngr *UserManager) GetUsers() ([]*User, error) {
	var users []*User
	for i, user := range mngr.cache {
		if i >= MAX_RETURN_ITEM_COUNT {
			break
		}
		users = append(users, user)
	}
	// XXX Do paging (plus real DB fetching) stuff to keep both caller and
	// memory happy
	return users, nil
}

func (mngr *UserManager) IsUserExist(user *User) bool {
	_, ok := mngr.names[user.Name]
	if !ok {
		var tmp User
		mngr.connMngr.Conn.QueryOne(tmp, `
			SELECT * FROM users WHERE name = (?)
		`, user.Name)
		ok = tmp.Id > 0
	}
	return ok
}

func (mngr *UserManager) insertUser2Cache(newUser *User) {
	newUser.time = time.Now().UnixNano()
	newUser.count = 1
	if mngr.cache.Len() >= USER_CACHE_SIZE {
		user := heap.Pop(&mngr.cache).(*User)
		delete(mngr.names, user.Name)
		delete(mngr.ids, user.Id)
		//glog.Printf("Age-out user %s (count: %d, time: %d) from memory cache.\n",
		//	user.Name, user.count, user.time)
	}
	heap.Push(&mngr.cache, newUser)
	mngr.names[newUser.Name] = newUser
	mngr.ids[newUser.Id] = newUser
}

func (mngr *UserManager) updateUserCache(user *User) {
	user.count++
	user.time = time.Now().UnixNano()
	heap.Fix(&mngr.cache, user.index)
}

func (mngr *UserManager) getRss4User(user *User) error {
	if user.rss != nil {
		// Relationship exist, do nothing
		return nil
	}

	var rss []Relationship
	_, err := mngr.connMngr.Conn.Query(&rss, `
		SELECT * FROM relationships WHERE user_id = (?id)
	`, user)
	if err != nil {
		glog.Errorf("Failed to find relationships for user(id: %d) in DB.\n", user.Id)
		return errors.New("find relationships in DB failed")
	}

	user.rss = make(MruRelationships, len(rss))
	for i := 0; i < len(rss); i++ {
		user.rss[i] = &rss[i]
		user.rss[i].Typ = TYPE_Relationship
		user.rss[i].count = 1
		user.rss[i].index = i
		user.rss[i].time = time.Now().UnixNano()
	}
	heap.Init(&user.rss)

	return nil
}

func (mngr *UserManager) GetUserById(id uint64) (*User, error) {
	if user, ok := mngr.ids[id]; ok {
		mngr.updateUserCache(user)
		return user, nil
	}

	var user *User
	_, err := mngr.connMngr.Conn.QueryOne(user, `
		SELECT * FROM users WHERE id = (?)
	`, id)
	if err != nil {
		glog.Errorf("Failed to find user(id: %d) in DB.\n", id)
		return nil, errors.New("find user in DB failed")
	}
	mngr.getRss4User(user)

	mngr.insertUser2Cache(user)
	return user, nil
}

func (mngr *UserManager) AddUser(newUser *User) (*User, error) {
	//glog.Println("Request for adding user, ", newUser.Name)

	if mngr.IsUserExist(newUser) {
		glog.Warningf("Trying to add an already exist user %s\n", newUser.Name)
		return nil, errors.New("user exists")
	}

	_, err := mngr.connMngr.Conn.QueryOne(newUser, `
		INSERT INTO users (name) VALUES (?name)
		RETURNING id
	`, newUser)

	mngr.insertUser2Cache(newUser)

	return newUser, err
}

func (mngr *UserManager) GetRelationships(idUser *User) ([]*Relationship, error) {
	user, err := mngr.GetUserById(idUser.Id)
	if err != nil {
		return nil, err
	}
	mngr.updateUserCache(user)

	if user.rss == nil {
		if err = mngr.getRss4User(user); err != nil {
			return nil, err
		}
	}

	var rss []*Relationship
	for i, rs := range user.rss {
		if i >= MAX_RETURN_ITEM_COUNT {
			break
		}
		rss = append(rss, rs)
	}

	return rss, nil
}

func (mngr *UserManager) updatePeerUserRsCache(rs *Relationship) {
	// Update peer user cache if loaded
	if user, ok := mngr.ids[rs.UserId]; ok {
		rsInCache := user.findRsInCache(rs)
		if rsInCache == nil {
			user.insertRs2User(rs)
		} else {
			rsInCache.State = rs.State
			user.updateRsCache(rsInCache)
		}
	}
}

func (mngr *UserManager) matchPeerRs(user *User, rs *Relationship) (*Relationship, bool) {
	if rs.State != StateLiked {
		return rs, false
	}

	// Update peer relationship to matched if the peer liked me already
	peerRs := Relationship{PeerId: user.Id, UserId: rs.PeerId, Typ: TYPE_Relationship}
	result, err := mngr.connMngr.Conn.QueryOne(&peerRs, `
			UPDATE relationships SET state = (?) 
			WHERE user_id = (?) AND peer_id = (?) AND state = (?) 
			RETURNING state
		`, StateMatched, rs.PeerId, user.Id, StateLiked)
	if err != nil {
		if result != nil {
			glog.Errorf("Update relationship failed, %s\n", err.Error())
		}
		return rs, false
	}

	if glog.V(2) {
		glog.Infof("DEBUG - peer rs after update: %d - %d - %d\n",
			peerRs.PeerId, peerRs.State, peerRs.UserId)
	}
	mngr.updatePeerUserRsCache(&peerRs)
	rs.State = StateMatched

	if glog.V(2) {
		glog.Infof("DEBUG - matched relationship after update: %d - %d - %d\n",
			rs.PeerId, rs.State, rs.UserId)
	}

	return rs, true
}

func (mngr *UserManager) breakupPeerRs(user *User, rs *Relationship) (*Relationship, bool) {
	if rs.State != StateDisliked {
		return rs, false
	}

	// Update peer relationship back to liked if the peer is matched
	peerRs := Relationship{PeerId: user.Id, UserId: rs.PeerId, Typ: TYPE_Relationship}
	_, err := mngr.connMngr.Conn.QueryOne(&peerRs, `
			UPDATE relationships SET state = (?) 
			WHERE user_id = (?) AND peer_id = (?) AND state = (?) 
			RETURNING state
		`, StateLiked, rs.PeerId, user.Id, StateMatched)
	if err != nil {
		return rs, false
	}

	if glog.V(2) {
		glog.Infof("DEBUG - peer rs after update: %d - %d - %d\n",
			peerRs.PeerId, peerRs.State, peerRs.UserId)
	}
	mngr.updatePeerUserRsCache(&peerRs)

	return rs, true
}

func (mngr *UserManager) isRelationshipExist(user *User, rs *Relationship) bool {
	targetRs := user.findRsInCache(rs)
	if targetRs != nil {
		return true
	}

	result, err := mngr.connMngr.Conn.QueryOne(targetRs, `
		SELECT * FROM relationships WHERE user_id = (?) AND peer_id = (?)
	`, user.Id, rs.PeerId)
	if err != nil {
		if result != nil {
			glog.Errorf("Failed to find relationships "+
				"for user(id: %d) in DB.\n", user.Id)
		}
		return false
	}
	return targetRs != nil && targetRs.PeerId > 0
}

func (mngr *UserManager) MarkRelationships(idUser *User, rs *Relationship) (*Relationship, error) {
	user, err := mngr.GetUserById(idUser.Id)
	if err != nil {
		return nil, err
	}
	mngr.updateUserCache(user)

	var markedRs *Relationship
	switch rs.State {
	default:
		errMsg := "No mark operation for state "
		errMsg += strconv.Itoa(rs.State)
		return nil, errors.New(errMsg)
	case StateLiked:
		markedRs, _ = mngr.matchPeerRs(user, rs)
	case StateDisliked:
		markedRs, _ = mngr.breakupPeerRs(user, rs)
	}

	if mngr.isRelationshipExist(user, markedRs) {
		_, err = mngr.connMngr.Conn.Exec(`
			UPDATE relationships SET state = (?) 
			WHERE peer_id = (?) AND user_id = (?)
		`, markedRs.State, markedRs.PeerId, markedRs.UserId)
		if err != nil {
			glog.Errorf(`Failed to update relationship for 
			user(id: %d) with peer(id: %d), %s.\n`,
				user.Id, rs.PeerId, err.Error())
			return nil, err
		}
	} else {
		_, err = mngr.connMngr.Conn.Exec(`
			INSERT INTO relationships (peer_id, state, user_id) VALUES (?, ?, ?)
		`, markedRs.PeerId, markedRs.State, markedRs.UserId)
		if err != nil {
			glog.Errorf(`Failed to insert relationship for 
			user(id: %d) with peer(id: %d), %s.\n`,
				user.Id, rs.PeerId, err.Error())
			return nil, err
		}
	}

	if rsInCache := user.findRsInCache(markedRs); rsInCache == nil {
		user.insertRs2User(markedRs)
	} else {
		rsInCache.State = markedRs.State
		user.updateRsCache(rsInCache)
	}

	return markedRs, nil
}
