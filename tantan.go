package main

import (
	"encoding/json"
	"flag"
	"net/http"
	"path"
	"strconv"

	"github.com/golang/glog"
	"github.com/gorilla/mux"
)

type Greeting struct {
	Hello string
}

func HelloWorld(writer http.ResponseWriter, request *http.Request) {
	greeting := &Greeting{Hello: "World"}
	b, err := json.Marshal(greeting)
	if err != nil {
		glog.Fatalln("Oops", err)
	}

	writer.Write(b)
}

const (
	TYPE_User         = "user"
	TYPE_Relationship = "relationship"
)

const httpStatusUnprocessableEntity = 422

var (
	connMngr *DBConnManager
	userMngr *UserManager
)

func UsersHandler(writer http.ResponseWriter, request *http.Request) {
	var resp []byte
	var err error

	switch request.Method {
	case "GET":
		users, err := userMngr.GetUsers()
		if err != nil {
			glog.Errorf("Fail to get all users, %s\n", err.Error())
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		resp, err = json.Marshal(users)
	case "POST":
		decoder := json.NewDecoder(request.Body)
		newUser := &User{Typ: TYPE_User}
		err := decoder.Decode(newUser)
		if err != nil {
			glog.Errorf("Cannot decode new user name, %s\n", err.Error())
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		if len(newUser.Name) == 0 {
			glog.Errorln("Invalid user adding request, no user name")
			writer.WriteHeader(httpStatusUnprocessableEntity)
			return
		}

		if exist := userMngr.IsUserExist(newUser); exist {
			glog.Warningf("Attempt to add an already exist user, %s\n",
				newUser.Name)
			writer.WriteHeader(http.StatusConflict)
			return
		}
		newUser, err = userMngr.AddUser(newUser)
		if err != nil {
			glog.Errorf("Fail to add new user(%s) with id(%d), %s\n",
				newUser.Name, newUser.Id, err.Error())
			writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		resp, err = json.Marshal(newUser)
		if err == nil {
			writer.WriteHeader(http.StatusCreated)
		}
	}

	if err != nil {
		glog.Errorf("Fail to serialize users to JSON, %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.Write(resp)
}

func RelationshipHandler(writer http.ResponseWriter, request *http.Request) {
	uIdStr := path.Base(path.Dir(request.URL.RequestURI()))
	uId, err := strconv.ParseUint(uIdStr, 10, 64)
	if err != nil {
		glog.Error("Invalid user id")
		writer.WriteHeader(httpStatusUnprocessableEntity)
		return
	}

	idUser := &User{Id: uId}
	rss, err := userMngr.GetRelationships(idUser)
	if err != nil || len(rss) == 0 {
		// Error, return empty
		writer.Write([]byte("[]"))
		return
	}

	resp, err := json.Marshal(rss)
	if err != nil {
		glog.Errorf("ERROR - Fail to serialize relationships to JSON, %s\n",
			err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.Write(resp)
}

func ChangeRelationship(writer http.ResponseWriter, request *http.Request) {
	peerIdStr := path.Base(request.URL.RequestURI())
	peerId, err := strconv.ParseUint(peerIdStr, 10, 64)
	if err != nil {
		glog.Errorln("ERROR - Invalid user id")
		writer.WriteHeader(httpStatusUnprocessableEntity)
		return
	}
	uIdStr := path.Base(path.Dir(path.Dir(request.URL.RequestURI())))
	uId, err := strconv.ParseUint(uIdStr, 10, 64)
	if err != nil {
		glog.Errorln("ERROR - Invalid user id")
		writer.WriteHeader(httpStatusUnprocessableEntity)
		return
	}

	decoder := json.NewDecoder(request.Body)
	rs := &Relationship{PeerId: peerId, Typ: TYPE_Relationship, UserId: uId}
	err = decoder.Decode(rs)
	if err != nil {
		glog.Errorf("Cannot decode new relationship state, %s\n",
			err.Error())
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	if glog.V(2) {
		glog.Infoln("Decoded relationship, ", rs)
	}

	idUser := &User{Id: uId}
	markedRs, err := userMngr.MarkRelationships(idUser, rs)
	if err != nil {
		glog.Errorf("Fail to mark relationship, %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp, err := json.Marshal(markedRs)
	if err != nil {
		glog.Errorf("Fail to serialize relationships to JSON, %s\n", err.Error())
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.Write(resp)
}

var configFile = flag.String("conf", "conf/db.json", "The configuration file name for database")

func prepare() {
	connMngr = NewDBConnManager(*configFile)
	userMngr = NewUserManager(connMngr)
}

func main() {
	port := flag.Int("port", 8081, "The port you want this server to listen on")
	flag.Parse()
	prepare()

	r := mux.NewRouter()
	r.HandleFunc("/", HelloWorld)
	r.HandleFunc("/users", UsersHandler).Methods("GET", "POST")
	s := r.PathPrefix("/users").Subrouter()
	s.HandleFunc("/", UsersHandler).Methods("GET", "POST")
	s.HandleFunc("/{id}/relationships/{other_id}", ChangeRelationship).Methods("PUT")
	s.HandleFunc("/{id}/relationships", RelationshipHandler).Methods("GET")
	http.Handle("/", r)

	glog.Info("Ready to serve requests")
	err := http.ListenAndServe(":"+strconv.Itoa(*port), nil)
	if err != nil {
		glog.Fatalf("Some one must stealed my port, %s\n", err.Error())
	}
	glog.Info("Stop to serve requests")
}
