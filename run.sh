#!/bin/bash

if [ ! -e "./tantan" ]; then
    echo Please run ./install.sh first
    exit 1
fi

./tantan -log_dir "/tmp" &
if [ ! $? == 0 ]; then
    echo Run server fail, please contact zhouhua015 at gmail.com
    exit 1
fi
PID=$!
echo tantan server is running with PID: $PID, please see /tmp/tantan.XXX for log during running

sleep 5

echo ========= add new user Alice =======
curl -XPOST -d '{"name":"Alice"}' -D - "http://localhost:8081/users"
echo 
echo 

echo ========= add one more new user Bob =======
curl -XPOST -d '{"name":"Bob"}' -D - "http://localhost:8081/users"
echo 
echo 

echo ========= let\'s how many users we got for now =======
curl -XGET -D - "http://localhost:8081/users"
echo 
echo 

IDs=$(curl -XGET -D - "http://localhost:8081/users" 2>/dev/null | tail -n 1 | tail -n 1 | cut -d, -f1,4)
AliceID=$(echo $IDs | cut -d, -f1 | cut -d: -f2)
BobID=$(echo $IDs | cut -d, -f2 | cut -d: -f2)

showRs4Alice() {
    echo ========= Alice\'s relationships =======
    curl -XGET -D - "http://localhost:8081/users/$AliceID/relationships"
    echo 
    echo 
}

showRs4Bob() {
    echo ========= Bob\'s relationships =======
    curl -XGET -D - "http://localhost:8081/users/$BobID/relationships"
    echo 
    echo 
}

echo ========= Alice liked Bob =======
curl -XPUT -d '{"state":"liked"}' -D - "http://localhost:8081/users/$AliceID/relationships/$BobID"
echo 
echo 

showRs4Bob

echo ========= Bob liked Alice back =======
curl -XPUT -d '{"state":"liked"}' -D - "http://localhost:8081/users/$BobID/relationships/$AliceID"
echo 
echo 

showRs4Alice

echo ========= Bob disliked Alice =======
curl -XPUT -d '{"state":"disliked"}' -D - "http://localhost:8081/users/$BobID/relationships/$AliceID"
echo 
echo 

showRs4Alice

echo ========= Bob liked Alice again =======
curl -XPUT -d '{"state":"liked"}' -D - "http://localhost:8081/users/$BobID/relationships/$AliceID"
echo 
echo 

showRs4Alice

echo All Done
kill $PID
