#!/bin/bash

go get github.com/gorilla/mux && \
go get github.com/golang/glog && \
go get gopkg.in/pg.v3
if [ ! $? == 0 ]; then
    echo Failed to get necessary go libraries 
    exit 1
fi

dropdb --if-exists tantan
createdb tantan
psql -Upostgres -f sql/db.sql tantan

curDir=$(basename `pwd`)
if [ $curDir != "tantan" ]; then
    echo Please run it under 'tantan' directory
    exit 1
fi
go build 
if [ ! $? == 0 ]; then
    echo Oops, failed compile, please contact zhouhua015 at gmail.com
    exit 1
fi
